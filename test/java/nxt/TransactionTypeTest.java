package nxt;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.util.TreeMap;

public class TransactionTypeTest {

    @Test
    public void genesisFee() {
        // Genesis block
        Assert.assertEquals(0, TransactionType.Payment.ORDINARY.calcFeeNQT(0, 0));

        // Normal blocks
        Assert.assertEquals(Constants.ONE_NXT, TransactionType.Payment.ORDINARY.calcFeeNQT(1, 0)); // Normal blocks
        Assert.assertEquals(Constants.ONE_NXT, TransactionType.Payment.ORDINARY.calcFeeNQT(100000, 0));

        // Another transaction type
        Assert.assertEquals(Constants.ONE_NXT, TransactionType.AccountControl.EFFECTIVE_BALANCE_LEASING.calcFeeNQT(100000, 0));

        // Asset creation
        Assert.assertEquals(1000 * Constants.ONE_NXT, TransactionType.ColoredCoins.ASSET_ISSUANCE.calcFeeNQT(1, 0));
        Assert.assertEquals(1000 * Constants.ONE_NXT, TransactionType.ColoredCoins.ASSET_ISSUANCE.calcFeeNQT(Constants.ASSET_EXCHANGE_BLOCK, 0));
    }

    @Test
    public void feeCalculation() {
        TransactionType type = new DummyTransactionType();
        Assert.assertEquals((2 + 1) * Constants.ONE_NXT, type.calcFeeNQT(2, 1000));
        Assert.assertEquals(2 * Constants.ONE_NXT, type.calcFeeNQT(1000, 2000));
    }

    @Test
    public void newTransactionFee() {
        TransactionType type = new DummyTransactionType();
        Assert.assertEquals(Constants.ONE_NXT, type.minimumFeeNQT(0, Integer.MAX_VALUE, 0, 1000));
        Assert.assertEquals(2 * Constants.ONE_NXT, type.minimumFeeNQT(1, Integer.MAX_VALUE, 0, 1000));
    }

    @Test
    public void dbTransactionFee() {
        TransactionType type = new DummyTransactionType();
        Assert.assertEquals(2 * Constants.ONE_NXT, type.minimumFeeNQT(0, 1500, 0, 2000));
    }

    @Test
    public void peerTransactionFee() {
        TransactionType type = new DummyTransactionType();
        Assert.assertEquals(Constants.ONE_NXT, type.minimumFeeNQT(0, Integer.MAX_VALUE, 1500, 1000));
        Assert.assertEquals(2 * Constants.ONE_NXT, type.minimumFeeNQT(1, Integer.MAX_VALUE, 1500, 2000));
        Assert.assertEquals((2 + 1) * Constants.ONE_NXT, type.minimumFeeNQT(1, Integer.MAX_VALUE, 50, 1000));
        Assert.assertEquals(2 * Constants.ONE_NXT, type.minimumFeeNQT(1, Integer.MAX_VALUE, 1000, 2000));
        Assert.assertEquals(2 * Constants.ONE_NXT, type.minimumFeeNQT(1, Integer.MAX_VALUE, 1960, 2000));
        Assert.assertEquals(3 * Constants.ONE_NXT, type.minimumFeeNQT(1, Integer.MAX_VALUE, 2060, 2000));
    }

    private static class DummyTransactionType extends TransactionType {

        @Override
        public TreeMap<Integer, Fee> getFeeLevels() {
            TreeMap<Integer, Fee> feeLevels = new TreeMap<>();
            feeLevels.put(0, new Fee(0, 0)); // Genesis block has no fee
            feeLevels.put(1, new Fee(2 * Constants.ONE_NXT, Constants.ONE_NXT / 1000));
            feeLevels.put(1000, new Fee(Constants.ONE_NXT, Constants.ONE_NXT / 2000));
            feeLevels.put(2000, new Fee(Constants.ONE_NXT, Constants.ONE_NXT / 1000));
            return feeLevels;
        }

        @Override
        public byte getType() {
            return 0;
        }

        @Override
        public byte getSubtype() {
            return 0;
        }

        @Override
        Attachment.AbstractAttachment parseAttachment(ByteBuffer buffer, byte transactionVersion) throws NxtException.ValidationException {
            return null;
        }

        @Override
        Attachment.AbstractAttachment parseAttachment(JSONObject attachmentData) throws NxtException.ValidationException {
            return null;
        }

        @Override
        void validateAttachment(Transaction transaction) throws NxtException.ValidationException {

        }

        @Override
        boolean applyAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
            return false;
        }

        @Override
        void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {

        }

        @Override
        void undoAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {

        }

        @Override
        void undoAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) throws UndoNotSupportedException {

        }

        @Override
        public boolean hasRecipient() {
            return false;
        }
    }

}
